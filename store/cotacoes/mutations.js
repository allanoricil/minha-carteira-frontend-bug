export default {
  FETCH_COTACOES: (state, payload) => {
    state.cotacoes = []
    payload.forEach((cotacao) => state.cotacoes.push(cotacao))
  }
}
