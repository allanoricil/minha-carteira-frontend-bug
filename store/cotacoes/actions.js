export default {
  async getCotacoes({ commit }, simbolo) {
    await this.$axios
      .$get('api/v1/cotacao/acao?simbolo=' + simbolo)
      .then((response) => {
        commit('FETCH_COTACOES', response)
      })
  }
}
