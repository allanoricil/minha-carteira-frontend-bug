export default {
  async getIndicesIbovespa({ commit }, dataInicial) {
    await this.$axios
      .$get('api/v1/ibov?dataInicial=' + dataInicial)
      .then((response) => {
        commit('FETCH_INDICES_IBOVESPA', response)
      })
  }
}
