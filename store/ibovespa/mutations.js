export default {
  FETCH_INDICES_IBOVESPA: (state, payload) => {
    state.indicesIbovespa = []
    payload.forEach((indiceIbovespa) =>
      state.indicesIbovespa.push(indiceIbovespa)
    )
  }
}
