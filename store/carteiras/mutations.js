export default {
  FETCH_CARTEIRAS: (state, payload) => {
    state.carteiras = []
    payload.forEach((carteira) => state.carteiras.push(carteira))
  },
  CADASTRA_CARTEIRA: (state, payload) => {
    state.carteiras.push(payload)
  }
}
