export default {
  FETCH_RESULTADOS_SWING_TRADE: (state, payload) => {
    console.log(payload)
    state.resultadosSwingTrade = []
    payload.forEach((resultado) => state.resultadosSwingTrade.push(resultado))
  },
  FETCH_RESULTADOS_DAY_TRADE: (state, payload) => {
    state.resultadosDayTrade = []
    payload.forEach((resultado) => state.resultadosDayTrade.push(resultado))
  },
  FETCH_RESULTADOS_CONSOLIDADO: (state, payload) => {
    state.resultadosConsolidado = []
    payload.forEach((resultado) => state.resultadosConsolidado.push(resultado))
  },
  FETCH_RETORNOS_SOBRE_INVESTIMENTO: (state, payload) => {
    state.retornosSobreInvestimento = []
    payload.forEach((retornoSobreInvestimento) =>
      state.retornosSobreInvestimento.push(retornoSobreInvestimento)
    )
  }
}
