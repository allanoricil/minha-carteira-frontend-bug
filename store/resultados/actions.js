export default {
  async getResultadosSwingTradeDaCarteira({ commit }, carteiraId) {
    await this.$axios
      .$get('/api/v1/resultado/swingtrade?carteiraId=' + carteiraId)
      .then((response) => {
        commit('FETCH_RESULTADOS_SWING_TRADE', response)
      })
  },
  async getResultadosDayTradeDaCarteira({ commit }, carteiraId) {
    await this.$axios
      .$get('/api/v1/resultado/daytrade?carteiraId=' + carteiraId)
      .then((response) => {
        commit('FETCH_RESULTADOS_DAY_TRADE', response)
      })
  },
  async getResultadosConsolidado({ commit }, carteiraId) {
    await this.$axios
      .$get('/api/v1/resultado/consolidado?carteiraId=' + carteiraId)
      .then((response) => {
        commit('FETCH_RESULTADOS_CONSOLIDADO', response)
      })
  },
  async getRetornosSobreInvestimento({ commit }, carteiraId) {
    await this.$axios
      .$get('/api/v1/resultado/roi?carteiraId=' + carteiraId)
      .then((response) => {
        commit('FETCH_RETORNOS_SOBRE_INVESTIMENTO', response)
      })
  }
}
