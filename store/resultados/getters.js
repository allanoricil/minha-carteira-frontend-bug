export default {
  getResultadosSwingTrade(state) {
    return JSON.parse(JSON.stringify(state.resultadosSwingTrade))
  },
  getResultadosDayTrade(state) {
    return JSON.parse(JSON.stringify(state.resultadosDayTrade))
  },
  getResultadosConsolidado(state) {
    return JSON.parse(JSON.stringify(state.resultadosConsolidado))
  },
  getRetornoSobreInvestimentoDadoUmProduto(state) {
    return (filter) =>
      state.retornosSobreInvestimento.filter((retornoSobreInvestimento) => {
        return retornoSobreInvestimento.simbolo === filter
      })[0]
  }
}
