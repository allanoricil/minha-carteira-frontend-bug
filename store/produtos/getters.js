export default {
  getProdutos(state) {
    return JSON.parse(JSON.stringify(state.produtos))
  },
  getProdutosComCapitalAlocado(state) {
    return JSON.parse(
      JSON.stringify(
        state.produtos.filter(
          (produto) => produto.precoMedio * produto.quantidadeAtual > 0
        )
      )
    )
  }
}
