export default {
  mode: 'spa',
  server: {
    port: 3000,
    host: '0.0.0.0'
  },
  env: {
    WS_URL: process.env.WS_URL || 'http://localhost:3000',
    REDIS_ENDPOINT: process.env.REDIS_ENDPOINT || 'http://localhost',
    REDIS_PORT: process.env.REDIS_PORT || 6379
  },
  /*
   ** Headers of the page
   */
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || ''
      },
      { name: 'theme-color', content: 'rgb(0, 0, 0)' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'stylesheet',
        type: 'text/css',
        href:
          'https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css'
      },
      {
        rel: 'stylesheet',
        type: 'text/css',
        href: 'https://fonts.googleapis.com/css?family=Roboto&display=swap'
      }
    ],
    script: [
      {
        src:
          'https://cdnjs.cloudflare.com/ajax/libs/socket.io/1.6.0/socket.io.min.js'
      },
      { src: 'https://code.jquery.com/jquery-3.3.1.slim.min.js' },
      {
        src:
          'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js'
      },
      {
        src:
          'https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js'
      }
    ]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: {
    color: 'blue',
    height: '0px'
  },
  /*
   ** Global CSS
   */
  css: ['~/assets/css/main.css'],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    { src: '~plugins/vue-chartjs.js', ssr: false },
    { src: '~plugins/clickOutEvent.js' }
  ],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
    '@nuxtjs/moment'
  ],
  moment: {
    defaultLocale: 'pt-br',
    locales: ['pt-br']
  },
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    'bootstrap-vue/nuxt',
    '@nuxtjs/moment',
    'nuxt-i18n',
    'nuxt-fontawesome',
    '~/io'
  ],

  i18n: {
    strategy: 'prefix_except_default',
    locales: ['pt-br'],
    defaultLocale: 'pt-br',
    vueI18n: {
      fallbackLocale: 'pt-br',
      messages: {
        'pt-br': {
          welcome: 'Ola'
        }
      }
    },
    parsePages: false,
    pages: {
      carteiras: false,
      operacoes: false,
      produtos: false
    }
  },

  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {
    proxy: true
  },
  proxy: {
    '/api/v1': 'http://localhost:8080'
  },
  /*
   ** Build configuration
   */
  build: {
    extend(config, ctx) {
      if (ctx.isDev) {
        config.devtool = ctx.isClient ? 'source-map' : 'inline-source-map'
      }
    }
  },
  serverMiddleware: ['~/api/index.js']
}
