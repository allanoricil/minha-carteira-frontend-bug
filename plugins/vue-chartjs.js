import Vue from 'vue'
import Chart from 'chart.js'
import { Bar, Doughnut, generateChart } from 'vue-chartjs'
import zoom from 'chartjs-plugin-zoom'

Chart.defaults.LineWithLine = Chart.defaults.line
Chart.controllers.LineWithLine = Chart.controllers.line.extend({
  draw(ease) {
    Chart.controllers.line.prototype.draw.call(this, ease)

    if (this.chart.tooltip._active && this.chart.tooltip._active.length) {
      const activePoint = this.chart.tooltip._active[0]
      const ctx = this.chart.ctx
      const x = activePoint.tooltipPosition().x
      const topY = this.chart.scales['y-axis-0'].top
      const bottomY = this.chart.scales['y-axis-0'].bottom

      const y = activePoint.tooltipPosition().y
      const leftX = this.chart.scales['x-axis-0'].left
      const rightX = this.chart.scales['x-axis-0'].right

      // draw line
      ctx.save()
      ctx.beginPath()
      ctx.moveTo(x, topY)
      ctx.lineTo(x, bottomY)
      ctx.lineWidth = 2
      ctx.strokeStyle = 'rgb(119, 122, 124)'
      ctx.setLineDash([5, 5])
      ctx.color = 'white'
      ctx.stroke()
      ctx.restore()

      // draw line
      ctx.save()
      ctx.beginPath()
      ctx.moveTo(leftX, y)
      ctx.lineTo(rightX, y)
      ctx.lineWidth = 2
      ctx.strokeStyle = 'rgb(119, 122, 124)'
      ctx.setLineDash([5, 5])
      ctx.color = 'white'
      ctx.stroke()
      ctx.restore()
    }
  }
})

const CustomLine = generateChart('custom-line', 'LineWithLine')

Vue.component('barChart', {
  extends: Bar,
  props: {
    data: {
      type: Object,
      default: () => {}
    },
    options: {
      type: Object,
      default: () => {}
    }
  },
  mounted() {
    this.addPlugin(zoom)
    this.renderChart(this.data, this.options)
  }
})

Vue.component('doughnutChart', {
  extends: Doughnut,
  props: {
    data: {
      type: Object,
      default: () => {}
    },
    options: {
      type: Object,
      default: () => {}
    }
  },
  mounted() {
    this.addPlugin({
      id: 'kwhWeek',
      afterDatasetDraw(chart) {
        const width = chart.chart.width
        const height = chart.chart.height
        const ctx = chart.chart.ctx

        ctx.restore()
        ctx.font = '2em Roboto'
        ctx.fillStyle = 'white'
        ctx.textBaseline = 'middle'

        const text =
          'R$ ' +
          chart.config.data.datasets[0].data
            .reduce((total, value) => total + parseFloat(value), 0)
            .toFixed(0)

        const div = document.createElement('div')
        div.innerHTML = text
        div.style.position = 'absolute'
        div.style.top = '-9999px'
        div.style.left = '-9999px'
        div.style.fontFamily = 'Roboto'
        div.style.fontWeight = 'bold'
        div.style.fontSize = '10pt' // or 'px'
        document.body.appendChild(div)
        const size = [div.offsetWidth, div.offsetHeight]
        document.body.removeChild(div)

        const textX = Math.round((width - ctx.measureText(text).width) / 2)
        const textY = height / 2 + 2 * size[1]

        ctx.fillText(text, textX, textY)
        ctx.save()
      }
    })
    this.addPlugin(zoom)
    this.renderChart(this.data, this.options)
  }
})

Vue.component('lineChart', {
  extends: CustomLine,
  props: {
    data: {
      type: Object,
      default: () => {}
    },
    options: {
      type: Object,
      default: () => {}
    }
  },

  mounted() {
    this.addPlugin(zoom)
    this.renderChart(this.data, this.options)
  }
})
