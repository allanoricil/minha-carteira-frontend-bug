import http from 'http'
import socketIO from 'socket.io'
import redis from 'redis'
import redisAdaper from 'socket.io-redis'

export default function() {
  this.nuxt.hook('render:before', (renderer) => {
    const server = http.createServer(this.nuxt.renderer.app)
    const io = socketIO(server)
    io.adapter(
      redisAdaper({
        host: process.env.REDIS_ENDPOINT,
        port: process.env.REDIS_PORT
      })
    )

    const subscriber = redis.createClient()
    const publisher = redis.createClient()

    // overwrite nuxt.server.listen()
    this.nuxt.server.listen = (port, host) =>
      new Promise((resolve) => {
        server.listen(port || 3000, host || 'localhost', resolve)
      })
    // close this server on 'close' event
    this.nuxt.hook('close', () => new Promise(server.close))

    io.sockets.on('connection', (socket) => {
      console.log(`Socket ${socket.id} connected.`)

      socket.on('disconnect', () => {
        console.log(`Socket ${socket.id} disconnected.`)
      })

      socket.on('notification', function(message) {
        socket.broadcast.emit('notification', message)
      })

      socket.on('create-room', (data) => {
        console.log("Got 'createRoom' from client , " + JSON.stringify(data))
        socket.join(data.room)
      })

      socket.on('join-room', (data) => {
        console.log(
          "Got 'joinRooom' from client " +
            socket.id +
            '  ' +
            JSON.stringify(data)
        )
        socket.join(data.room)
        io.to(data.room).emit('teste-message', 'teste')

        io.of('/').adapter.clients((err, clients) => {
          console.log(clients)
          if (err) console.log(err)
        })

        io.in(data.room).clients((err, clients) => {
          console.log(clients)
          if (err) console.log(err)
        })

        io.of('/').adapter.clientRooms(socket.id, (err, rooms) => {
          if (err) console.log(err)
          console.log('ROOMS THE SOCKET ID IS IN:')
          console.log(rooms)
        })
      })

      socket.on('teste-message', (data) => {
        console.log(data)
      })

      socket.on('send-message', (data) => {
        console.log('Sending Message: ' + JSON.stringify(data))
        publisher.publish(data.uuid, data.message)
      })
    })

    subscriber.on('notification', (channel, message) => {
      io.sockets.emit('notification', message)
    })
  })
}
